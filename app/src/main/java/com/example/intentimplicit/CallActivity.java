package com.example.intentimplicit;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class CallActivity extends AppCompatActivity {
    private static final int REQUEST_CALL = 101;
    private TextView tvCall, tvDial;
    private EditText edtPhone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.call_activity);
        initViews();
        initEvents();
        if (checkSelfPermission(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED){
            Toast.makeText(this, "Thank you", Toast.LENGTH_SHORT).show();
        } else {
            requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL);
        }
    }

    private void initEvents() {
        tvCall.setOnClickListener(v -> doCall());
        tvDial.setOnClickListener(v -> doDial());
    }

    private void doDial() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + edtPhone.getText().toString()));
        startActivity(intent);
    }

    private void doCall() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + edtPhone.getText().toString()));
        startActivity(intent);
    }

    private void initViews() {
        tvCall = findViewById(R.id.tv_call);
        edtPhone = findViewById(R.id.edt_phone);
        tvDial = findViewById(R.id.tv_dial);
    }
}
