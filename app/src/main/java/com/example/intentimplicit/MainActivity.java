package com.example.intentimplicit;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

public class MainActivity extends AppCompatActivity {
    private static final int PICK_IMAGE = 1;
    ImageView imageView;
    TextView tvPick, tvCapture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        initAction();
    }

    private void initViews() {
        imageView = findViewById(R.id.imv_image);
        tvCapture = findViewById(R.id.tv_capture);
        tvPick = findViewById(R.id.tv_pick_image);
    }

    private void initAction() {
        tvCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doCapture();
            }
        });

        tvPick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doPick();
            }
        });
    }

    private void doCapture() {
    }

    private void doPick() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(
                Intent.createChooser(intent, "Select Image"), PICK_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            Glide.with(this)
                    .load(uri)
                    .placeholder(R.drawable.ic_baseline_image_24)
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(imageView);
        }
    }
}